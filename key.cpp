#if 0
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endif
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
using namespace std;
namespace CopyRP
{
    class Key
    {
        public:
            string _CDKey = "";
            string _HexData = "";
            uint8_t _ByteData[15];
            bool SkipCleaning = false;
            bool SkipChecks = false;
            bool UseDivider = true;
            char CDKeyDivider = '-';
            char HexDataDivider = ',';

            string CDKey() {
                return _CDKey;
            }

            void CDKey(const string& _value) { 
                string value;
                string validChars = "0123456789ABCDEFGHJKLMNPRSTVWXYZ";
                bool validFormat = true;
                if (!SkipCleaning)
                {
                    for(auto c: _value) {
                        switch(c) {
                            case '-':
                            case ' ':
                            break;
                            case 'O': value += '0';
                            break;
                            case 'I': value += '1';
                            break;
                            default:
                                      value += std::toupper(c);
                        }
                    }
                    //                    value = value.Replace("-", "");
                    //                    value = value.Replace(",", "");
                    //                    value = value.Replace(" ", "");
                    //                    value = value.Replace("O", "0");
                    //                    value = value.Replace("I", "1");
                } else {
                    value = _value;
                }

                if (!SkipChecks)
                {
                    for (int i = 0; i < value.size() && validFormat; i++)
                    {
                        char currentChar = value[i];
                        bool validChar;
                        if(auto ii = validChars.find(currentChar) != std::string::npos) {
                            validChar = true;
                        } else {
                            validChar = false;
                        }
                        if (!validChar) validFormat = false;
                    }
                }

                if (validFormat)
                {
                    std::memset(_ByteData, 0x0, sizeof(_ByteData));
                    for (uint i = 0; i < 3; i++)
                    {
                        uint64_t BitwiseResult = 0;
                        for (uint ii = 0; ii < 8; ii++)
                        {
                            BitwiseResult |= validChars.find(value[i * 8 + ii]) << (ii * 5);
                        }
                        for (uint ii = 0; ii < 5; ii++)
                        {
                            _ByteData[i * 5 + 5 - 1 - ii] += static_cast<uint8_t>(BitwiseResult & 0xff);
                            BitwiseResult >>= 8;
                        }
                    }
                    std::ostringstream out;
                    for(auto c: _ByteData) {
                        out << std::hex << int(c) << ",";
                    }
                    _HexData = out.str();
                    //_HexData = BitConverter.ToString(_ByteData);
#if 0
                    if (UseDivider)
                    {
                        if (HexDataDivider != '-') _HexData = _HexData.Replace('-', HexDataDivider);
                        value = value.Insert(4, new string(CDKeyDivider, 1));
                        value = value.Insert(10, new string(CDKeyDivider, 1));
                        value = value.Insert(16, new string(CDKeyDivider, 1));
                        value = value.Insert(22, new string(CDKeyDivider, 1));
                    }
                    else
                    {
                        _HexData = _HexData.Replace('-', '\0');
                    }
#endif                    
                    _CDKey = value;
                }
            }
            string HexData() {
                return _HexData;
            }
            void HexData(const string& _value) 
            {
                std::string value;
                string validHexChars = "0123456789ABCDEF";
                string validKeyChars = "0123456789ABCDEFGHJKLMNPRSTVWXYZ";
                bool validFormat = true;
                if (!SkipCleaning)
                {
                    for(auto c: _value)
                        switch(c) {
                            case '-':
                            case ',':
                            case ' ':
                                break;
                            default:
                                value += std::toupper(c);
                        }
//                        value = value.ToUpper();
//                    value = value.Replace("-", "");
//                    value = value.Replace(",", "");
//                    value = value.Replace(" ", "");
                } else {
                    value = _value;
                }
                if (!SkipChecks)
                {
                    for (int i = 0; i < value.size(); i++)
                    {
                        char currentChar = value[i];
                        bool validChar = false;
                        for (int ii = 0; ii < validHexChars.size() && !validChar; ii++) if (currentChar == validHexChars[ii]) validChar = true;
                        if (!validChar) validFormat = false;
                    }
                }
                if (validFormat)
                {
                    for(int idx=0; idx < value.size(); idx+=2) {
                        _ByteData[idx<<1] = std::strtoul(value.substr(idx,2).c_str(), 0, 16);
                    }
//                    _ByteData = Enumerable.Range(0, value.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(value.Substring(x, 2), 16)).ToArray();
                    _CDKey = "";
                    for (int i = 0; i < 3; i++)
                    {
                        uint64_t BitwiseResult = 0;
                        for (int ii = 0; ii != 5; ii++)
                        {
                            BitwiseResult = _ByteData[i * 5 + ii] | (BitwiseResult << 8);
                        }
                        for (int ii = 0; ii != 8; ii++)
                        {
                            _CDKey += validKeyChars[(int)(BitwiseResult & 0x1f)];
                            BitwiseResult >>= 5;
                        }
                    }
                    std::ostringstream out;
                    for(auto c : _ByteData) {
                        out << std::hex << c << " ";
                    }

                    _HexData  = out.str();
#if 0
                    if (UseDivider)
                    {
                        if (HexDataDivider != '-') _HexData = _HexData.Replace('-', HexDataDivider);
                        _CDKey = _CDKey.Insert(4, new string(CDKeyDivider, 1));
                        _CDKey = _CDKey.Insert(10, new string(CDKeyDivider, 1));
                        _CDKey = _CDKey.Insert(16, new string(CDKeyDivider, 1));
                        _CDKey = _CDKey.Insert(22, new string(CDKeyDivider, 1));
                    }
                    else
                    {
                        _HexData = _HexData.Replace('-', '\0');
                    }
#endif
                }
            }
    };
}


int main(int argc, char** argv) {
    CopyRP::Key key;
    if(argc >= 2) {
        key.CDKey(argv[1]);
        std::cout << "CD :  " << key.CDKey() << "\n";
        std::cout << "HEX : " << key.HexData() << "\n";
    }

    return 0;
}
